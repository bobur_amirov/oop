
class Employee:
    def __init__(self, first_name, last_name, age, pay):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.email = self.first_name + self.last_name + '@gmail.com'
        self.pay = pay

class Manager(Employee):
    def __init__(self, first_name, last_name, age, pay, add_employee=None):
        super().__init__(first_name, last_name, age, pay)
        if add_employee is None:
            self.add_employee = None
        else:
            self.add_employee = add_employee

    def add_emp(self, emp):
        self.add_employee.append(emp)

    def info(self):
        for emp in self.add_employee:
            print(f'emp add {emp.first_name} {emp.last_name}')

emp1 = Employee('bobur', 'amirov', 25, 15000)
emp2 = Employee('bobur1', 'amirov2', 25, 12000)
emp3 = Employee('Bobur', 'Amirov', 25, 10000)

man = Manager('Boburjon', 'Amirov', 26, 30000, [emp2])
man.add_emp(emp1)
man.info()