class BankAccount:
    def __init__(self):
        self.balance = 0

    def deposit(self, amount):
        self.balance += amount

    def withdraw(self, amount):
        self.balance -= amount

class MinBalance(BankAccount):
    def __init__(self, min_balance):
        BankAccount.__init__(self)
        self.min_balance = min_balance

    def withdraw(self, amount):
        if self.balance - amount < self.min_balance:
            print('Siz bu summani olish uchun pul yetarli emas iltimos kamroq summa kiriting')
        else:
            self.balance -= amount

bobur = MinBalance(50)
bobur.deposit(90)
print(bobur.balance)
bobur.withdraw(50)
print(bobur.balance)

