# blog yaratish va u necha marta kurilganini hisoblash uchun misol
# class Blog:
#     def __init__(self, author, name, text, baho):
#         self.author = author
#         self.name = name
#         self.text = text
#         self.k = 0
#         self.baho = baho

#     def get_info(self):
#         return f'{self.k} marta kurilgan'

#     def views(self):
#         obj.k+=1


# obj = Blog('Bobur', 'Python oop', 'Thid is python oop', 4)
# obj.views()
# obj.views()
# obj.views()
# print(f'{obj.k} marta kurilgan')
# print(obj.get_info())
# =====================================================

# oop da overloading ga misol
# class A:
#     def add(self, *args):
#         result = 0
#         for i in args:
#             result += i
#         return result

# obj = A()
# print(obj.add(2,0))
# print(obj.add(2,4,10,12,16))
# print(obj.add(2,4,6))
# print(obj.add(2,4,6,8))
# =================================

# set va get methodlar bilan ishlash
# class B:
#     def __init__(self, num):
#         self.num = num

#     def set_value(self, value):
#         self.num = value

#     def get_value(self):
#         return self.num

# obj = B(23)
# print(obj.num)
# obj.set_value(53)
# print(obj.get_value())

# =========================================

class C:
    a = 10
    _b = 20
    __c = 30
    def get_info(self):
        print(self.a)
        print(self._b)
        print(self.__c)

obj = C()
obj.get_info()
print(obj.a)
print(obj._b)
print(obj.__c) # __c pirevate class atribut bulgani uchun class dan tashqarida murojat qilib bulmaydi

